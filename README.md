# Base64images #

### This extension has viewhelpers which can be used for reduce http request related to images. ###

There are 2 viewhelpers included in it. Both are extending from fluid viewhelpers.

1. f:image
1. f:uri.image

### So you can use all functionalities/attributes which is provided by fluid viewhelpers. ###

# How to use it #

1. Include namespace of extension in your fluid template.

{namespace mb = ManthanB\Base64images\ViewHelpers}

2. Use this viewhelpers with additional parameter ### convert ###
### Example ###
* <mb:image src="path to your image" convert='1' />
* <img src="{mb.imageUri(src:'path to your image', convert:1)}" />